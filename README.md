# Simplest Kanban Board

Single-paged Kanban board for agile teams with capacity planning, task tracking and progress indicator.

You can also keep track of tasks with a deadline and receive alerts.

# Roadmap

Integration with Bitbucket

